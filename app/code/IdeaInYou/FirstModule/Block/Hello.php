<?php

namespace IdeaInYou\FirstModule\Block;

use IdeaInYou\Review\Api\ReviewRepositoryInterface;
use IdeaInYou\Review\Model\ResourceModel\Review\CollectionFactory;
use IdeaInYou\Review\Model\ReviewFactory;
use Magento\Framework\View\Element\Template;

class Hello extends \Magento\Framework\View\Element\Template
{
    private CollectionFactory $collectionFactory;
    private ReviewRepositoryInterface $reviewRepository;
    private ReviewFactory $reviewFactory;

    public function __construct(
        ReviewFactory $reviewFactory,
        ReviewRepositoryInterface $reviewRepository,
        CollectionFactory $collectionFactory,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->reviewRepository = $reviewRepository;
        $this->reviewFactory = $reviewFactory;
    }

    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set('My Title');
        return parent::_prepareLayout();
    }

    public function getHelloMessage()
    {
        return 'hello from block';
    }

    public function getReviewCollection()
    {
        return $this->collectionFactory->create();
    }

    public function getReviewById($reviewId) {
        return $this->reviewRepository->getById($reviewId);
    }

    public function createReview() {
        $review = $this->reviewFactory->create();
        $review->setAuthor('sss')
            ->setProductId(1)
            ->setContent('zxczxcasd' . time());
        $this->reviewRepository->save($review);
    }
}